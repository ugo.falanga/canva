//
//  ViewController.swift
//  Canva
//
//  Created by Ugo Falanga on 10/12/2019.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit

class HomeController: UIViewController {
    var selectedItem : [Int] = [-1,-1]

    lazy var searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 300, height: 20))
    
    let galleryNewPost: [UIImage] = [
        UIImage(named: "1")!,
        UIImage(named: "2")!,
        UIImage(named: "3")!,
        UIImage(named: "4")!,
        UIImage(named: "5")!,
        UIImage(named: "6")!
    ]
    
    let galleryAnimated: [UIImage] = [
        UIImage(named: "animated01")!,
        UIImage(named: "animated02")!,
        UIImage(named: "animated03")!,
        UIImage(named: "animated04")!,
        UIImage(named: "animated05")!,
        UIImage(named: "animated06")!,
        UIImage(named: "animated07")!,
        UIImage(named: "animated08")!,
        UIImage(named: "animated09")!,
        UIImage(named: "animated10")!,
        UIImage(named: "animated11")!
    ]
    
    let galleryPost: [UIImage] = [
        UIImage(named: "post01")!,
        UIImage(named: "post02")!,
        UIImage(named: "post03")!,
        UIImage(named: "post04")!,
        UIImage(named: "post05")!,
        UIImage(named: "post06")!,
        UIImage(named: "post07")!,
        UIImage(named: "post08")!,
        UIImage(named: "post09")!,
        UIImage(named: "post10")!
    ]
    
    let galleryStories: [UIImage] = [
        UIImage(named: "stories01")!,
        UIImage(named: "stories02")!,
        UIImage(named: "stories03")!,
        UIImage(named: "stories04")!,
        UIImage(named: "stories05")!,
        UIImage(named: "stories06")!,
        UIImage(named: "stories07")!,
        UIImage(named: "stories08")!,
        UIImage(named: "stories09")!,
        UIImage(named: "stories10")!
    ]
    
    let galleryLogo: [UIImage] = [
        UIImage(named: "logo01")!,
        UIImage(named: "logo02")!,
        UIImage(named: "logo03")!,
        UIImage(named: "logo04")!,
        UIImage(named: "logo05")!,
        UIImage(named: "logo06")!,
        UIImage(named: "logo07")!,
        UIImage(named: "logo08")!,
        UIImage(named: "logo09")!,
        UIImage(named: "logo10")!
    ]
    
    let galleryLeaflet: [UIImage] = [
        UIImage(named: "leaflet01")!,
        UIImage(named: "leaflet02")!,
        UIImage(named: "leaflet03")!,
        UIImage(named: "leaflet04")!,
        UIImage(named: "leaflet05")!,
        UIImage(named: "leaflet06")!,
        UIImage(named: "leaflet07")!,
        UIImage(named: "leaflet08")!,
        UIImage(named: "leaflet09")!,
        UIImage(named: "leaflet10")!
    ]
    
    let galleryPosters: [UIImage] = [
        UIImage(named: "poster01")!,
        UIImage(named: "poster02")!,
        UIImage(named: "poster03")!,
        UIImage(named: "poster04")!,
        UIImage(named: "poster05")!,
        UIImage(named: "poster06")!,
        UIImage(named: "poster07")!,
        UIImage(named: "poster08")!,
        UIImage(named: "poster09")!,
        UIImage(named: "poster10")!
    ]
    
    
    
    
    var scrollView = UIScrollView()
    
    // Creation of different type of collection view
    fileprivate let collectionViewNewProject: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(CollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        return cv
    }()
    
    
    fileprivate let collectionViewAnimatedSocial: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(CollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        return cv
    }()
    
    
    fileprivate let collectionViewInstagramPost: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(CollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        return cv
    }()
    
    
    fileprivate let collectionViewInstagramStories: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(CollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        return cv
    }()
    
    fileprivate let collectionViewLogo: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(CollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        return cv
    }()
    
    fileprivate let collectionViewLeaflet: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(CollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        return cv
    }()
    
    fileprivate let collectionViewPosters: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(CollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        return cv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.placeholder = "Search into 60.000 models"
        let leftNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.navigationController?.navigationBar.isTranslucent = false
        self.tabBarController?.tabBar.setValue(true, forKey: "hidesShadow")
        self.tabBarController?.tabBar.isTranslucent = false
        
        setupScrollView()
        setupNewProject()
        setupAnimatedSocial()
        setupInstagramPost()
        setupInstagramStories()
        setupLogo()
        setupLeaflet()
        setupPosters()
        // Do any additional setup after loading the view.
    }
    
    // Create scroll view
    func setupScrollView() {
        scrollView.frame = CGRect(x: 0, y: 20, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width, height: 1700)
        
        view.addSubview(scrollView)
        
        let spaceView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 1))
        scrollView.addSubview(spaceView)
        
        scrollView.keyboardDismissMode = .interactive
        
    }
    
    // Create all the collection view
    func setupNewProject() {
        let titleLabel = UILabel(frame: CGRect(x: 20, y: (scrollView.subviews.last?.frame.maxY)!+20, width: scrollView.contentSize.width, height: 20))
        titleLabel.text = "What project do you want to create today?"
        titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        scrollView.addSubview(titleLabel)
        
        let subtitleLable = UILabel(frame: CGRect(x: 20, y: (scrollView.subviews.last?.frame.maxY)!, width: scrollView.contentSize.width, height: 20))
        subtitleLable.text = "Create a new project"
        subtitleLable.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        scrollView.addSubview(subtitleLable)
        
        collectionViewNewProject.frame = CGRect(x: 0, y: (scrollView.subviews.last?.frame.maxY)!, width: view.frame.width, height: 160)
        scrollView.addSubview(collectionViewNewProject)
        collectionViewNewProject.backgroundColor = .clear
        collectionViewNewProject.delegate = self
        collectionViewNewProject.dataSource = self
        collectionViewNewProject.tag = 1
        collectionViewNewProject.showsHorizontalScrollIndicator = false
        collectionViewNewProject.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        
    }
    
    func setupAnimatedSocial() {
        let titleLabel = UILabel(frame: CGRect(x: 20, y: ((scrollView.subviews.last?.frame.maxY)!)+20, width: scrollView.contentSize.width, height: 20))
        titleLabel.text = "Animated social media"
        titleLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        scrollView.addSubview(titleLabel)
        
        let showButton = UIButton(frame: CGRect(x: scrollView.frame.width-70, y: titleLabel.frame.minY-5, width: 60, height: 30))
        showButton.setTitle("Show all", for: .normal)
        showButton.setTitleColor(.systemGray, for: .normal)
        showButton.backgroundColor = .clear
        showButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .light)
        showButton.addTarget(self, action: #selector( showButtonPressed ), for: .touchUpInside)
        scrollView.addSubview(showButton)
        
        collectionViewAnimatedSocial.frame = CGRect(x: 0, y: (scrollView.subviews.last?.frame.maxY)!, width: view.frame.width, height: 160)
        scrollView.addSubview(collectionViewAnimatedSocial)
        collectionViewAnimatedSocial.backgroundColor = .clear
        collectionViewAnimatedSocial.delegate = self
        collectionViewAnimatedSocial.dataSource = self
        collectionViewAnimatedSocial.tag = 2
        collectionViewAnimatedSocial.showsHorizontalScrollIndicator = false
        collectionViewAnimatedSocial.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }
    
    func setupInstagramPost() {
        let titleLabel = UILabel(frame: CGRect(x: 20, y: ((scrollView.subviews.last?.frame.maxY)!)+20, width: scrollView.contentSize.width, height: 20))
        titleLabel.text = "Instagram post"
        titleLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        scrollView.addSubview(titleLabel)
        
        let showButton = UIButton(frame: CGRect(x: scrollView.frame.width-70, y: titleLabel.frame.minY-5, width: 60, height: 30))
        showButton.setTitle("Show all", for: .normal)
        showButton.setTitleColor(.systemGray, for: .normal)
        showButton.backgroundColor = .clear
        showButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .light)
        showButton.addTarget(self, action: #selector( showButtonPressed ), for: .touchUpInside)
        scrollView.addSubview(showButton)
        
        collectionViewInstagramPost.frame = CGRect(x: 0, y: (scrollView.subviews.last?.frame.maxY)!, width: view.frame.width, height: 160)
        
        scrollView.addSubview(collectionViewInstagramPost)
        collectionViewInstagramPost.backgroundColor = .clear
        collectionViewInstagramPost.delegate = self
        collectionViewInstagramPost.dataSource = self
        collectionViewInstagramPost.tag = 3
        collectionViewInstagramPost.showsHorizontalScrollIndicator = false
        collectionViewInstagramPost.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        
    }
    
    func setupInstagramStories() {
        let titleLabel = UILabel(frame: CGRect(x: 20, y: ((scrollView.subviews.last?.frame.maxY)!)+20, width: scrollView.contentSize.width, height: 20))
        titleLabel.text = "Instagram stories"
        titleLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        scrollView.addSubview(titleLabel)
        
        let showButton = UIButton(frame: CGRect(x: scrollView.frame.width-70, y: titleLabel.frame.minY-5, width: 60, height: 30))
        showButton.setTitle("Show all", for: .normal)
        showButton.setTitleColor(.systemGray, for: .normal)
        showButton.backgroundColor = .clear
        showButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .light)
        showButton.addTarget(self, action: #selector( showButtonPressed ), for: .touchUpInside)
        scrollView.addSubview(showButton)
        
        collectionViewInstagramStories.frame = CGRect(x: 0, y: (scrollView.subviews.last?.frame.maxY)!, width: view.frame.width, height: 160)
        
        scrollView.addSubview(collectionViewInstagramStories)
        collectionViewInstagramStories.backgroundColor = .clear
        collectionViewInstagramStories.delegate = self
        collectionViewInstagramStories.dataSource = self
        collectionViewInstagramStories.tag = 4
        collectionViewInstagramStories.showsHorizontalScrollIndicator = false
        collectionViewInstagramStories.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        
    }
    
    func setupLogo() {
        let titleLabel = UILabel(frame: CGRect(x: 20, y: ((scrollView.subviews.last?.frame.maxY)!)+20, width: scrollView.contentSize.width, height: 20))
        titleLabel.text = "Logo"
        titleLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        scrollView.addSubview(titleLabel)
        
        let showButton = UIButton(frame: CGRect(x: scrollView.frame.width-70, y: titleLabel.frame.minY-5, width: 60, height: 30))
        showButton.setTitle("Show all", for: .normal)
        showButton.setTitleColor(.systemGray, for: .normal)
        showButton.backgroundColor = .clear
        showButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .light)
        showButton.addTarget(self, action: #selector( showButtonPressed ), for: .touchUpInside)
        scrollView.addSubview(showButton)
        
        collectionViewLogo.frame = CGRect(x: 0, y: (scrollView.subviews.last?.frame.maxY)!, width: view.frame.width, height: 160)
        
        scrollView.addSubview(collectionViewLogo)
        collectionViewLogo.backgroundColor = .clear
        collectionViewLogo.delegate = self
        collectionViewLogo.dataSource = self
        collectionViewLogo.tag = 5
        collectionViewLogo.showsHorizontalScrollIndicator = false
        collectionViewLogo.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        
    }
    
    func setupLeaflet() {
        let titleLabel = UILabel(frame: CGRect(x: 20, y: ((scrollView.subviews.last?.frame.maxY)!)+20, width: scrollView.contentSize.width, height: 20))
        titleLabel.text = "Leaflet"
        titleLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        scrollView.addSubview(titleLabel)
        
        let showButton = UIButton(frame: CGRect(x: scrollView.frame.width-70, y: titleLabel.frame.minY-5, width: 60, height: 30))
        showButton.setTitle("Show all", for: .normal)
        showButton.setTitleColor(.systemGray, for: .normal)
        showButton.backgroundColor = .clear
        showButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .light)
        showButton.addTarget(self, action: #selector( showButtonPressed ), for: .touchUpInside)
        scrollView.addSubview(showButton)
        
        collectionViewLeaflet.frame = CGRect(x: 0, y: (scrollView.subviews.last?.frame.maxY)!, width: view.frame.width, height: 160)
        
        scrollView.addSubview(collectionViewLeaflet)
        collectionViewLeaflet.backgroundColor = .clear
        collectionViewLeaflet.delegate = self
        collectionViewLeaflet.dataSource = self
        collectionViewLeaflet.tag = 6
        collectionViewLeaflet.showsHorizontalScrollIndicator = false
        collectionViewLeaflet.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        
    }
    
    func setupPosters() {
        let titleLabel = UILabel(frame: CGRect(x: 20, y: ((scrollView.subviews.last?.frame.maxY)!)+20, width: scrollView.contentSize.width, height: 20))
        titleLabel.text = "Posters"
        titleLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        scrollView.addSubview(titleLabel)
        
        let showButton = UIButton(frame: CGRect(x: scrollView.frame.width-70, y: titleLabel.frame.minY-5, width: 60, height: 30))
        showButton.setTitle("Show all", for: .normal)
        showButton.setTitleColor(.systemGray, for: .normal)
        showButton.backgroundColor = .clear
        showButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .light)
        showButton.addTarget(self, action: #selector( showButtonPressed ), for: .touchUpInside)
        scrollView.addSubview(showButton)
        
        collectionViewPosters.frame = CGRect(x: 0, y: (scrollView.subviews.last?.frame.maxY)!, width: view.frame.width, height: 160)
        
        scrollView.addSubview(collectionViewPosters)
        collectionViewPosters.backgroundColor = .clear
        collectionViewPosters.delegate = self
        collectionViewPosters.dataSource = self
        collectionViewPosters.tag = 7
        collectionViewPosters.showsHorizontalScrollIndicator = false
        collectionViewPosters.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        
    }
    
    @objc func showButtonPressed(sender: UIButton){
        print("button pressed")
        performSegue(withIdentifier: "segueButtonShow", sender: self)
    }
}


extension HomeController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        print("Clic tag = \(collectionView.tag)  row = \(indexPath.row)")
        self.selectedItem = [collectionView.tag, indexPath.row]
        performSegue(withIdentifier: "segueId", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueId" {
            let controller = segue.destination as! CellSegueViewController
        
            controller.title = ""
            if (self.selectedItem.first == 1){
                controller.imageTemp = self.galleryNewPost[self.selectedItem.last!]
            }
            else if (self.selectedItem.first == 2){
                controller.imageTemp = self.galleryAnimated[self.selectedItem.last!]
            }
            else if (self.selectedItem.first == 3){
                controller.imageTemp = self.galleryPost[self.selectedItem.last!]
            }
            else if (self.selectedItem.first == 4){
                controller.imageTemp = self.galleryStories[self.selectedItem.last!]
            }
            else if (self.selectedItem.first == 5){
                controller.imageTemp = self.galleryLogo[self.selectedItem.last!]
            }
            else if (self.selectedItem.first == 6){
                controller.imageTemp = self.galleryLeaflet[self.selectedItem.last!]
            }
        }
    }
    
    // Size of cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize(width: 0, height: 0)
        
        if collectionView.tag == 1 {
            if(indexPath.row == 0){
                size = CGSize(width: 80, height: 80)
            } else if(indexPath.row == 1){
                size = CGSize(width: 80, height: 80)
            } else if(indexPath.row == 2){
                size = CGSize(width: 70, height: 120)
            } else if(indexPath.row == 3){
                size = CGSize(width: 80, height: 80)
            } else {
                size = CGSize(width: 76, height: 105)
            }
        }
        else if collectionView.tag == 2 {
            size = CGSize(width: 120, height: 120)
        }
        else if collectionView.tag == 3 {
            size = CGSize(width: 120, height: 120)
        }
        else if collectionView.tag == 4 {
            size = CGSize(width: 70, height: 120)
        }
        else if collectionView.tag == 5 {
            size = CGSize(width: 120, height: 120)
        }
        else if collectionView.tag == 6 {
            size = CGSize(width: 80, height: 110)
        }
        else if collectionView.tag == 7 {
            size = CGSize(width: 76, height: 110)
        }
        return size
    }
    
    // Number of cell
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var itemInSection = 0
        
        if(collectionView.tag == 1){
            itemInSection = galleryNewPost.count
        }
        else if(collectionView.tag == 2){
            itemInSection = galleryAnimated.count
        }
        else if(collectionView.tag == 3){
            itemInSection = galleryPost.count
        }
        else if(collectionView.tag == 4){
            itemInSection = galleryStories.count
        }
        else if(collectionView.tag == 5){
            itemInSection = galleryLogo.count
        }
        else if(collectionView.tag == 6){
            itemInSection = galleryLeaflet.count
        }
        else if(collectionView.tag == 7){
            itemInSection = galleryPosters.count
        }
        return itemInSection
    }
    
    // Generate cell for collection view
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
        if(collectionView.tag == 1){
            cell.backgroundColor = .systemGray3
            cell.layer.cornerRadius = 10
            cell.layer.shadowColor = UIColor.systemGray5.cgColor
            cell.layer.shadowOffset = CGSize(width: 4, height: 4)
            cell.layer.shadowRadius = 5
            cell.layer.shadowOpacity = 1
            if(indexPath.row == 0){
                cell.layer.position.y = collectionView.contentSize.height-50
            } else if(indexPath.row == 1){
                cell.layer.position.y = collectionView.contentSize.height-50
            } else if(indexPath.row == 2){
                cell.layer.position.y = collectionView.contentSize.height-70
            } else if(indexPath.row == 3){
                cell.layer.position.y = collectionView.contentSize.height-50
            } else {
                cell.layer.position.y = collectionView.contentSize.height-65
            }
            
            cell.setImage(image: galleryNewPost[indexPath.row])
        }
        else if(collectionView.tag == 2){
            cell.backgroundColor = .white
            cell.layer.cornerRadius = 10
            cell.layer.shadowColor = UIColor.systemGray5.cgColor
            cell.layer.shadowOffset = CGSize(width: 4, height: 4)
            cell.layer.shadowRadius = 5
            cell.layer.shadowOpacity = 1
            cell.setImage(image: galleryAnimated[indexPath.row])
        }
        else if (collectionView.tag == 3) {
            cell.backgroundColor = .white
            cell.layer.cornerRadius = 10
            cell.layer.shadowColor = UIColor.systemGray5.cgColor
            cell.layer.shadowOffset = CGSize(width: 4, height: 4)
            cell.layer.shadowRadius = 5
            cell.layer.shadowOpacity = 1
            cell.setImage(image: galleryPost[indexPath.row])
            
        }
        else if (collectionView.tag == 4) {
            cell.backgroundColor = .white
            cell.layer.cornerRadius = 10
            cell.layer.shadowColor = UIColor.systemGray5.cgColor
            cell.layer.shadowOffset = CGSize(width: 4, height: 4)
            cell.layer.shadowRadius = 5
            cell.layer.shadowOpacity = 1
            cell.setImage(image: galleryStories[indexPath.row])
            
        }
        else if (collectionView.tag == 5) {
            cell.backgroundColor = .white
            cell.layer.cornerRadius = 10
            cell.layer.shadowColor = UIColor.systemGray5.cgColor
            cell.layer.shadowOffset = CGSize(width: 4, height: 4)
            cell.layer.shadowRadius = 5
            cell.layer.shadowOpacity = 1
            cell.setImage(image: galleryLogo[indexPath.row])
        }
        else if (collectionView.tag == 6){
            cell.backgroundColor = .white
            cell.layer.cornerRadius = 10
            cell.layer.shadowColor = UIColor.systemGray5.cgColor
            cell.layer.shadowOffset = CGSize(width: 4, height: 4)
            cell.layer.shadowRadius = 5
            cell.layer.opacity = 1
            cell.setImage(image: galleryLeaflet[indexPath.row])
        }
        else if (collectionView.tag == 7){
            cell.backgroundColor = .white
            cell.layer.cornerRadius = 10
            cell.layer.shadowColor = UIColor.systemGray5.cgColor
            cell.layer.shadowOffset = CGSize(width: 4, height: 4)
            cell.layer.shadowRadius = 5
            cell.layer.opacity = 1
            cell.setImage(image: galleryPosters[indexPath.row])
        }
        
        return cell
    }
    
}
