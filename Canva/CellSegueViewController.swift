//
//  CellSegueViewController.swift
//  Canva
//
//  Created by Ugo Falanga on 15/12/2019.
//  Copyright © 2019 Apple Inc. All rights reserved.
//

import UIKit

class CellSegueViewController: UIViewController {
    @IBOutlet weak var imageSelected: UIImageView!
    
    public var imageTemp: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(imageSelected)
        
        imageSelected.image = imageTemp
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
